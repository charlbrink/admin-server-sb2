package za.brinkc.admin;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "sba")
public class RoleConfigurationProperties {
        private String[] allowedRoles = new String[0];
}
