package za.brinkc.admin;

import de.codecentric.boot.admin.server.config.AdminServerProperties;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.logout.RedirectServerLogoutSuccessHandler;
import org.springframework.security.web.server.authentication.logout.ServerLogoutSuccessHandler;

import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties({RoleConfigurationProperties.class})
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@EnableAutoConfiguration
@EnableAdminServer
@EnableDiscoveryClient
@EnableScheduling
@RequiredArgsConstructor
@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class SpringBootAdminServerApplication {
    private static final String RESOURCE_ACCESS = "resource_access";
    public static final String ROLES = "roles";
    private final AdminServerProperties adminServer;
    private final RoleConfigurationProperties roleProperties;

    public static void main(final String[] args) {
        SpringApplication.run(SpringBootAdminServerApplication.class, args);
    }

    @Bean
    @Profile("insecure")
    public SecurityWebFilterChain securityWebFilterChainPermitAll(ServerHttpSecurity http) {
        return http.authorizeExchange((authorizeExchange) -> authorizeExchange.anyExchange().permitAll())
                .csrf(ServerHttpSecurity.CsrfSpec::disable).build();
    }

    @Bean
    @Profile("secure")
    public SecurityWebFilterChain securityWebFilterChainSecure(ServerHttpSecurity http) {
        return http
                .authorizeExchange((authorizeExchange) -> authorizeExchange
                        .pathMatchers("/actuator/info", "/actuator/health").permitAll()
                        .pathMatchers(this.adminServer.path("/assets/**")).permitAll()
                        .pathMatchers(this.adminServer.path("/login")).permitAll()
                        .pathMatchers(HttpMethod.GET, this.adminServer.getContextPath()+"/**")
                        .hasAnyAuthority(roleProperties.getAllowedRoles())
                        .anyExchange().authenticated())
                .logout(logout -> logout.logoutUrl(this.adminServer.path("/logout")).logoutSuccessHandler(serverLogoutSuccessHandler()))
                .oauth2Login(Customizer.withDefaults())
                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .build();
    }

    @Bean
    public GrantedAuthoritiesMapper userAuthoritiesMapper() {
        return (authorities) -> {
            Set<GrantedAuthority> mappedAuthorities = new HashSet<>();

            authorities.forEach(authority -> {
                if (authority instanceof OidcUserAuthority) {
                    OidcUserAuthority oidcUserAuthority = (OidcUserAuthority)authority;

                    OidcIdToken idToken = oidcUserAuthority.getIdToken();
                    OidcUserInfo userInfo = oidcUserAuthority.getUserInfo();
                    if (log.isDebugEnabled()) {
                        log.debug("Preferred username [{}]", userInfo.getPreferredUsername());
                        log.debug("attributes [{}]", oidcUserAuthority.getAttributes().get(RESOURCE_ACCESS));
                        log.debug("claims [{}]", idToken.getClaims().toString());
                    }

                    // Map the claims found in idToken and/or userInfo
                    // to one or more GrantedAuthority's and add it to mappedAuthorities
                    var resourceAccess = oidcUserAuthority.getAttributes().get(RESOURCE_ACCESS);
                    if (resourceAccess instanceof Map) {
                        var authorizedParty = ((Map<String, Object>)resourceAccess).get(idToken.getAuthorizedParty());
                        if (authorizedParty instanceof Map) {
                            var roles = ((Map<String, Object>)authorizedParty).get(ROLES);
                            if (roles instanceof List) {
                                mappedAuthorities.addAll(((List<String>)roles).stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()));
                            }
                        }
                    }
                }
            });

            return mappedAuthorities;
        };
    }

    private ServerLogoutSuccessHandler serverLogoutSuccessHandler() {
        RedirectServerLogoutSuccessHandler serverLogoutSuccessHandler =
                new RedirectServerLogoutSuccessHandler();
        serverLogoutSuccessHandler.setLogoutSuccessUrl(URI.create(this.adminServer.path("/")));
        return serverLogoutSuccessHandler;
    }
}
