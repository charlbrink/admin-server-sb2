# README #

Sample reactive Spring Boot Admin with secured with Keycloak.

###Keycloak
```
docker run -d --name keycloak -p 8000:8080 \
   -e KEYCLOAK_USER=spring \
   -e KEYCLOAK_PASSWORD=spring123 \
   jboss/keycloak
```

In the Keycloak Administration Console at http://localhost:8000/auth/
* Create the Realm "brinkc-cloud"
* Set Access Type to "confidential"
* Set Valid Redirect URIs to "*"
* In the Credentials tab get the Secret and update the client-secret in application-local.yml
    ```
    spring.security.oauth2.client.registration.payments-portal.client-secret: 
    ```
* Create Client "sba", Authentication Flow Overrides - browser + direct grant
* Add Roles "SBA-USER", "SBA-ADMIN"
* Add mapper User Client Role

###
Run locally with profiles local,insecure or local,secure

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
